package com.imprint_nfc.sendtoserial;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
    TextView error;
    private Button mButtonStartRepeatedTask;
    private Button mButtonStopRepeatedTask;
    private Handler mHandler;
    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonStartRepeatedTask = (Button) findViewById(R.id.start_repeat_intervals);
        mButtonStopRepeatedTask = (Button) findViewById(R.id.stop_repeat_intervals);
        final EditText mEditText = (EditText) findViewById(R.id.time_interval);
        error = (TextView) findViewById(R.id.error);

        mHandler = new Handler();

        // Листенер для кнопки запуска повторяющихся операций
        mButtonStartRepeatedTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int time_interval = 1000 * Integer.parseInt(mEditText.getText().toString());

                // Инициализируем новый Runnable
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        TextView main_text = (TextView) findViewById(R.id.textView);
                        try {
                            String sharedFact = main_text.getText().toString();
                            Process process = Runtime.getRuntime().exec("sh"); //даем привелегии и создаем процесс
                            OutputStream os = process.getOutputStream(); //получаем поток процесса
                            DataOutputStream dos = new DataOutputStream(os); //создаем объект класса потока
                            String cmd = "echo '" + sharedFact + "\r\n'> /dev/ttyS0";
                            error.setText(dos.toString());
                            dos.writeBytes(cmd); //пишем байты
                            dos.writeBytes("echo '" + new String(new byte[]{(byte) 29, (byte) 86, (byte) 48}) + "\r\n'> /dev/ttyS0");
                            dos.flush(); //проталкиваем байты
                            dos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            error.setText(e.toString());
                        }

                        // Повторять с определенным интервалом
                        mHandler.postDelayed(this, time_interval);
                    }
                };
                // Выполнить задачу через время
                mHandler.postDelayed(mRunnable, 1000);
            }
        });

        // Листенер для остановки повторяющейся операции
        mButtonStopRepeatedTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHandler.removeCallbacks(mRunnable);
            }
        });
    }

    public void send_command(View view) {
        TextView main_text = (TextView) findViewById(R.id.textView);
        try {
            String sharedFact = main_text.getText().toString();
            Process process = Runtime.getRuntime().exec("sh"); //даем привелегии и создаем процесс
            OutputStream os = process.getOutputStream(); //получаем поток процесса
            DataOutputStream dos = new DataOutputStream(os); //создаем объект класса потока
            String cmd = "echo '" + sharedFact + "\r\n'> /dev/ttyS0";
            error.setText(dos.toString());
            dos.writeBytes(cmd); //пишем байты
            dos.flush(); //проталкиваем байты
            dos.close();
        } catch (Exception e) {
            e.printStackTrace();
            error.setText(e.toString());
        }
    }


    public void send_command_special(View view) {
        try {
//            Process process = Runtime.getRuntime().exec("su"); //даем привелегии SU и создаем процесс
            Process process = Runtime.getRuntime().exec("sh");//создаем оболочку (по сути поток)
            OutputStream os = process.getOutputStream(); //получаем поток процесса
            DataOutputStream dos = new DataOutputStream(os); //создаем объект класса потока
            error.setText(dos.toString());
            dos.writeBytes("echo '" + new String(new byte[]{(byte) 29, (byte) 86, (byte) 48}) + "\r\n'> /dev/ttyS0");
            dos.flush(); //проталкиваем байты
            dos.close();
        } catch (Exception e) {
            e.printStackTrace();
            error.setText(e.toString());
        }
    }

    //Посылка бинарного файла по пути объявленном в file переменной через echo
    public void send_command_echo(View view) {
        try {
            //Read binary file
            File file = new File("/Data/local/1");
            byte[] fileData = new byte[(int) file.length()];

            try {
                DataInputStream dis = new DataInputStream(new FileInputStream(file));
                dis.readFully(fileData);
                dis.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                error.setText("File not found! Motherfucker!");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                error.setText(e.toString());
            }

            //Run command shell and execture echo command
            Process proc = Runtime.getRuntime().exec("sh");
            OutputStream stream = proc.getOutputStream();
            DataOutputStream dos = new DataOutputStream(stream);
            error.setText(dos.toString());
            dos.writeBytes("echo '" + new String(fileData) + "'> /dev/ttyS0");
            dos.flush(); //проталкиваем байты
            dos.close();
        } catch (Exception e) {
            error.setText(e.toString());
        }
    }

    //Посылать с помощью скомпилированных сишных библиотек
    public void send_command_binary_file(View view) {
        try {
            File fin = new File("/dev/ttymxc0");
            SerialPort sp = new SerialPort(fin, 115200, 0);
            OutputStream stream = sp.getOutputStream();
            error.setText(stream.toString());
            stream.write(new String("text").getBytes());
            stream.write('\n');
            stream.flush();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
            error.setText(e.toString());
        }
    }

}
